import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';
import './src/styles/style.scss';

import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.js';


class App extends React.Component {
  render() {
    return (
         <div>
            Hi! The boilerplate is working.
			Start building.
         </div>
      );
  }
}
ReactDOM.render(<App/>, document.getElementById('app'));
